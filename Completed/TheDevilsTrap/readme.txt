The Devils' Trap

The game is a text based game.The objective of the game is to complete the mission provided to you, 
i.e. to collect the food for your community from the world above ruled by The Devil and it's demons.
In order to collect the food the player has to complete a series of tasks along the way.
--------------------------------------------------------------------------------------------------------
I. Folder/File List

Debug  				theDevilsTrap.vcxproj
Devil.cpp			theDevilsTrap.vcxproj.filters 

--------------------------------------------------------------------------------------------------------
II. Prerequisites

System requirements
OS: Windows 7 SP1+, 8, 10, 64-bit versions only


Software required 
Visual Studio Community (version 2015 and above)

Download Visual Studio Community

https://www.visualstudio.com/downloads/
--------------------------------------------------------------------------------------------------------
III. Issues

The game currently runs on the console of Visual Studios.
While carrying out one of the tasks, the game might enter into an infinite loop based on the choice of 
the user. (It occurs rarely, could not find the source of the problem.)

The small bugs will be addressed in the coming versions along with inclusion of graphics.
--------------------------------------------------------------------------------------------------------
IV. Acknowledgement

Shrey Arora(Team-mate)
Shreyas M K(Team-mate)
Reference books on C++
--------------------------------------------------------------------------------------------------------


